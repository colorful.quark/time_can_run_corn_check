
<?php
define("H23_H12", ["23", "22", "21", "20", "19", "18", "17", "16", "15", "14", "13", "12"]);
define("H00_H06", ["00","01","02","03","04","05","06"]);
define("BANK_LIST", ['01' => 'bbl', '02' => 'kbank', '03' => 'ktb', '04' => 'ttb', '05' => 'scb', '06' => 'bay', '07' => 'gsb', '08' => 'tbank', '09' => 'baac', '10' => 'ghb', '1' => 'bbl', '2' => 'kbank', '3' => 'ktb', '4' => 'ttb', '5' => 'scb', '6' => 'bay', '7' => 'gsb', '8' => 'tbank', '9' => 'baac', '11' => 'citi', '12' => 'scbl', '13' => 'isbt', '14' => 'kk', '15' => 'tcd', '16' => 'lh', '17' => 'cimb', '18' => 'uob']);
define("BANK_STMT_LIST", ['bbl' => ['01','1'], 'kbank' => ['02','2'], 'ktb' => ['03','3'], 'ttb' => ['04','4'], 'scb' => ['05','5'], 'bay' => ['06','6'], 'gsb' => ['07','7'], 'tbank' => ['08','8'], 'baac' => ['09','9'], 'ghb' => ['10'], 'citi' => ['11'], 'scbl' => ['12'], 'isbt' => ['13'], 'kk' => ['14'], 'tcd' => ['15'], 'lh' => ['16'], 'cimb' => ['17'], 'uob' => ['18']]);

date_default_timezone_set("Asia/Bangkok");

// $st = '00.00';
// $et = '05.00';
// $ct = '19.19';

// $res_old = oldAlg($s, $e, $c);
// $res_new = newAlg($s, $e, $c);

// if ($res_old == $res_new)
// {
//     echo "result: matched!";
// } else {
//     echo "result: conflict!";
// }

// echo "<br> start:{$st} end:{$et} current:{$ct}";
// echo "<br>old_algo: {$res_old} <br> new_algo:{$res_new}";
// echo "<br>========================";
// die;

$t_start = '00.00';
$modify = '+30 mins';

// counters
$c = 0;
$C = 0;
for ($st = $t_start; $st < '24.00'; $st = modify_time($st, $modify)) {
    for ($et = $t_start; $et < '24.00'; $et = modify_time($et, $modify)) {
        for ($ct = $t_start; $ct < '24.00'; $ct = modify_time($ct, $modify)) {
            $res_old = oldAlg($st, $et, $ct);
            $res_new = newAlg($st, $et, $ct);
            $C++;
            if ($res_old == $res_new){
                // echo "result matched!";
                continue;
            } else {
                if ($st < $et) {
                    // $c++;
                    // echo "<br>result: conflict! ";
                    // echo "<br> start: {$st} end: {$et} current: {$ct}";
                    // echo "<br>old_algo: {$res_old} <br> new_algo: {$res_new}";
                    // echo "<br>========================";
                    // break;
                }
                if ($st === $et) {
                    continue;
                }
                $c++;
                echo "<br>result: conflict! ";
                echo "<br> start:{$st} end:{$et} current:{$ct}";
                echo "<br>old_algo: {$res_old} <br> new_algo:{$res_new}";
                echo "<br>========================";
                // break;
            }
        }
    }
}

echo "<br>Total conflict: {$c}/{$C}";


/** 
 * @param string $stime
 * @param string $etime
 * @param string $ctime
 * @return string
 */
function oldAlg($stime, $etime, $ctime)
{
    $start_time_can_not_deposit = new DateTime($stime);
    $end_time_can_not_deposit = new DateTime($etime);
    // $setting_end_time = $rs['end_time_can_not_deposit'];
        
    $time_current = new DateTime($ctime);

    $chk_can_run_cron = true;

    // Check proper time to run corn,
    // If start time (can not deposit) is in H23_H12
    if (in_array($start_time_can_not_deposit->format("H"), H23_H12)) {
        
        // If end time not in H23_H12 && current time not in H00_H06, extend 1 day to end time
        if (!in_array($end_time_can_not_deposit->format("H"), H23_H12) &&
                !in_array($time_current->format("H"), H00_H06))
        {
            $end_time_can_not_deposit = $end_time_can_not_deposit->add(new DateInterval('P1D'));
            // echo "end time adjusted <br>";
        }

        // If current time is in after H00_H06, subtract 1 day to start time
        if (!in_array($time_current->format("H"), H23_H12) &&
                in_array($time_current->format("H"), H00_H06))
        { 
            $start_time_can_not_deposit = $start_time_can_not_deposit->sub(new DateInterval('P1D'));
            // echo "start time adjusted <br>";
        }

        // If period of time doesn't make sense, don't run
        if ($start_time_can_not_deposit->getTimestamp() > $end_time_can_not_deposit->getTimestamp()) {
            // echo json_encode(['status' => false, "message" => "Start-End Time ignore check 1"]);
            $chk_can_run_cron = false;
            //exit();

        // If current time is in bank off period, don't run
        } else if ($time_current->getTimestamp() >= $start_time_can_not_deposit->getTimestamp() &&
                    $time_current->getTimestamp() <= $end_time_can_not_deposit->getTimestamp())
        {
            // echo json_encode(['status'=>false,"message" => "Start-End Time ignore check 2"]);
            $chk_can_run_cron = false;
            //exit();
        }

    // Normal case
    } else if ($start_time_can_not_deposit->getTimestamp() <= $end_time_can_not_deposit->getTimestamp()) {

        // echo "normal case no time adjusted <br>";
        // If current time is in bank off interval, don't run
        if ($time_current->getTimestamp() >= $start_time_can_not_deposit->getTimestamp() &&
            $time_current->getTimestamp() <= $end_time_can_not_deposit->getTimestamp()) 
        {
            // echo json_encode(['status'=>false,"message" => "Start-End Time ignore check 3"]);
            $chk_can_run_cron = false;
            //exit();
        }
    }

    return $chk_can_run_cron === true ? 'true' : 'false';
}

/** 
 * @param string $stime
 * @param string $etime
 * @param string $ctime
 * @return string 
 */
function newAlg($stime, $etime, $ctime)
{
    $start_time_can_not_deposit = new DateTime($stime);
    $end_time_can_not_deposit = new DateTime($etime);
        
    $time_current = new DateTime($ctime);

    $chk_can_run_cron = true;

    if ($start_time_can_not_deposit > $end_time_can_not_deposit) {
        if ($end_time_can_not_deposit >= $time_current) {
            $start_time_can_not_deposit = $start_time_can_not_deposit->modify("-1 day");
        } else {
            $end_time_can_not_deposit = $end_time_can_not_deposit->modify("+1 day");
        }
    }

    $chk_can_run_cron = !is_in_time_period($start_time_can_not_deposit, $end_time_can_not_deposit, $time_current);

    return $chk_can_run_cron === true ? 'true' : 'false';
}

/**
 * @param DateTime $stime
 * @param DateTime $etime
 * @param DateTime $now
 * @return bool
 */
function is_in_time_period($stime, $etime, $now) {

    if ($stime <= $now && $now <= $etime) {
        return true;
    }

    return false;
}

/**
 * @param string $time
 * @param string $modify
 * @return string
 */
function modify_time($time, $modify)
{
    $dt = new DateTime($time);
    $dt->modify($modify);
    $res = $dt->format('H:i');
    if ($time > $res) {
        return '24.00';
    }
    return $res;
}
